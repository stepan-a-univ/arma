\begin{frame}
  \frametitle{First order autoregressive  model}
  \framesubtitle{Definition}

  \begin{block}{AR(1) model}
    Let $\{\epsilon_t, t\in\mathbb Z\}$ be a univariate Gaussian white
    noise with 0 mean and variance $\sigma_{\epsilon}^2$. The
    stochastic process $\{y_t,t\in\mathbb Z\}$ is a first order
    autoregressive process, $\{y_t\}\sim AR(1)$, if
    \begin{equation}\label{eq:ar:1:def}\tag{AR-1}
    y_t = c +  \varphi y_{t-1} + \epsilon_t
    \end{equation}
    with $c\in\mathbb R$ and $\varphi\in (-1,1)$.
  \end{block}
  \begin{itemize}
  \item We assume that \eqref{eq:ar:1:def} defines a stationary
    stochastic process, and will provide conditions ensuring
    stationarity later.
  \item The main property of this model is that the dependency between $y_t$ and $y_{t-h}$ declines geometrically with $h$.
  \item $y_t$ and $y_{t-h}$, are dependent for all $h$, but the
    weights associated to the common innovations converge to zero as
    $h$ goes to infinity.
  \item[$\Rightarrow$] $\gamma_y(h)\neq 0 \quad \forall h\in \mathbb Z$ but $\lim_{h\rightarrow\infty}\gamma(h)=0$.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{First order autoregressive model}
  \framesubtitle{Moments}

  The unconditional expectation of a first order autoregressive process, as defined
  in \ref{eq:ar:1:def}, is $\mu = \nicefrac{c}{1-\varphi}$.\newline

  \bigskip

  \begin{block}{AR(1) autocovariance function}
    The autocovariance function of a first order autoregressive
    process, as defined in \ref{eq:ar:1:def}, is given by:
    \[
    \begin{split}
      \gamma_y(0) &= \frac{\sigma_{\epsilon}^2}{1-\varphi^2}\\
      \gamma_y(h) &= \varphi\gamma_y(h-1),\quad \forall h\neq 0\\
    \end{split}
    \]
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Finite order autoregressive model}
  \framesubtitle{Definition}

  \begin{block}{AR(p) model}
    Let $\{\epsilon_t, t\in\mathbb Z\}$ be a univariate strong white
    noise with 0 mean and variance $\sigma_{\epsilon}^2$. The
    stochastic process $\{y_t,t\in\mathbb Z\}$ is a $p<\infty$ order
    autoregressive process, $\{y_t\}\sim AR(q)$, if
    \begin{equation}\label{eq:ar:p:def}\tag{AR-p}
    y_t = c  + \sum_{i=1}^p\varphi_i y_{t-i}+  \epsilon_t
    \end{equation}
    with $c$ and $\varphi_i$ $\in\mathbb R$.
  \end{block}
  \begin{itemize}
  \item We assume that the stochastic process described by
    \eqref{eq:ar:p:def} is stationary, later we will show how to impose restrictions on the coefficients
    $\varphi_i$ to ensure stationary.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Finite order autoregressive model}
  \framesubtitle{Moments}

  The expectation of an AR($p$) process, as defined
  in \ref{eq:ar:p:def}, is $\mu = \nicefrac{c}{1-\varphi_1-\dots-\varphi_p}$.\newline

  \begin{block}{AR($p$) autocovariance function}
    The autocovariance function of a finite order autoregressive
    process, as defined in \ref{eq:ar:p:def}, must solve:
    {\tiny
    \[
    \begin{cases}
      \gamma_y(0) &= \varphi_1\gamma_y(1) + \varphi_2\gamma_y(2) +
      \dots + \varphi_p\gamma_y(p) + \sigma_{\epsilon}^2\\
      \gamma_y(1) &= \varphi_1\gamma_y(0) + \varphi_2\gamma_y(1) +
      \dots + \varphi_p\gamma_y(p-1)\\
      &\vdots\\
      \gamma_y(h) &= \varphi_1\gamma_y(h-1) + \varphi_2\gamma_y(h-2) +
      \dots + \varphi_p\gamma_y(h-p)\\
      &\vdots\\
      \gamma_y(p) &= \varphi_1\gamma_y(p-1) + \varphi_2\gamma_y(p-2) +
      \dots + \varphi_p\gamma_y(0)
    \end{cases}
    \]}
    And for $|h|>p$ we use the following recursion:
    {\tiny
    \[
    \gamma_y(h) = \varphi_1\gamma_y(h-1) + \varphi_2\gamma_y(h-2) +
      \dots + \varphi_p\gamma_y(h-p)
    \]}
  \end{block}
\end{frame}


\begin{notes}
  The system of linear equations given in the previous slide is known
  as the system of Yule-Walker equations. This system can be solved
  for the autoregressive parameters, if the autocovariance function is
  known, or the autocovariance function if the parameters are
  known.\newline

  If we want to solve for the autocovariance function, we can rewrite
  the first and last equations as:
  \[
  \gamma_y(0) - \varphi_1\gamma_y(1) - \varphi_2\gamma_y(2) -
  \dots - \varphi_p\gamma_y(p) = \sigma_{\epsilon}^2
  \]
  \[
  -\varphi_p\gamma_y(0)-\varphi_{p-1}\gamma_y(1)-\dots-\varphi_1\gamma_y(p-1)+\gamma_y(p) =0
  \]
  and the $p-1$ intermediary equations as:
  \[
  -\varphi_h\gamma(0) -
  \left(\varphi_{h-1}+\varphi_{h+1}\right)\gamma_y(1)-\dots-\left(\varphi_{1}+\varphi_{2h-1}\right)\gamma_y(h-1)
  +(1-\varphi_{2h})\gamma_y(h)-\varphi_{2h+1}\gamma_y(h+1)-\dots-\varphi_p\gamma(p-h)=0
  \]
  for $h=1,\dots,p-1$. Using matrix notations, we need to solve:
  \[
  \begin{pmatrix}
    1          & -\varphi_1                               & -\varphi_2& \dots&\dots&\dots&\dots& -\varphi_p\\
    -\varphi_1 & 1-\varphi_2                 & -\varphi_3    &\dots&\dots&\dots& & -\varphi_p\\
    \vdots     &                                          &               &&&& & \vdots\\
    -\varphi_h  &
    -\varphi_{h-1}-\varphi_{h+1}  &
    \dots &
    -\varphi_{1}-\varphi_{2h-1}&
    1-\varphi_{2h}&
    -\varphi_{2h+1}&
    \dots&
    -\varphi_p\\
    \vdots     &                                          &               &&&& & \vdots\\
    -\varphi_p & -\varphi_{p-1}                             &  -\varphi_{p-2} &\dots&\dots&\dots&\dots& 1
  \end{pmatrix}
  \begin{pmatrix}
    \gamma_{y}(0)\\
    \gamma_{y}(1)\\
    \vdots\\
    \gamma_{y}(h)\\
    \vdots
    \gamma_{y}(p)\\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \sigma_{\epsilon}^2\\
    0\\
    \vdots\\
    0\\
    \vdots\\
    0
  \end{pmatrix}
\]
for $\gamma_y(h)$, $h=0,\dots,p$. The autocovariance is computed by
inverting a $(p+1)\times (p+1)$.
\end{notes}


% Local Variables:
% TeX-master: "slides.tex"
% ispell-check-comments: exclusive
% ispell-local-dictionary: "british"
% End: